from random import randint

min_year = 1924
max_year = 2004



min_month = 1
max_month = 12



#Prompt "Hi! What is your name?"
#Guess <<guess number>> : <<name>> were you born in <<m> / <<yyyy>>?
#Prompt "yes or no?"
# If yes,
    #I knew it
#else if
    #Drat! Lemme try again!
#else
    #I have other things to do. Goodbye
num_of_guesses = input('How many guesses do I get?')
num_of_guesses = int(num_of_guesses)
ask_name = input('Hi! What is your name?')
name = ask_name
name_greeting = print(f'Hi {name}!')

for guess_num in range(num_of_guesses):
    rand_year = randint(min_year,max_year)
    rand_month = randint(min_month, max_month)
    print(f'Guess {guess_num+1} : {name} were you born in {rand_month}/{rand_year} ?')
    response = input('yes or no?')
    if response == "yes":
        print('I knew it!')
        exit()
    elif guess_num == (num_of_guesses-1):
        print('I have other things to do. Good bye.')
    else:
        print('Drat! Lemme try again!')
